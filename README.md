# RBC Robots #

This is a solution proposal for the RBC programming assignment.

### Technology Stack ###

* Java 1.8
* Spring Boot v1.5.10
* AngularJS v1.6.4
* Bootstrap v4

### How to run localy ? ###

* Clone the repository on your local machine
* run `mvn -q clean spring-boot:run`
* Visit http://localhost:8080/

### How is the app structured ? ###
* `Robot` the model representation of a robot
* `RobotRepository` the service to handle database queries
* `RobotCommandeLineRunner` to feed the app with a list of basic robots
* `RobotController` the REST controller handling all API requests
* `WebController` to render the index view template using the thymeleaf templating engine

### Deployment ###
Live version : https://rbc-robots.herokuapp.com/

The app is hosted on Heroku, to deploy it to production you can run `git push heroku master`
(Guides : https://devcenter.heroku.com/articles/deploying-spring-boot-apps-to-heroku)
