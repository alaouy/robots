package com.rbc.robot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
public class RobotCommandeLineRunner implements CommandLineRunner {
    private final RobotRepository repository;

    public RobotCommandeLineRunner(RobotRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run (String... strings) throws Exception {
        repository.save(new Robot("Robot 1", 20, 13.0, "https://images-na.ssl-images-amazon.com/images/I/41OkAAcxYeL.jpg"));
        repository.save(new Robot("Robot 2", 10, 24.0, "https://multimedia.bbycastatic.ca/multimedia/products/500x500/104/10416/10416244.jpg"));
        repository.save(new Robot("Robot 3", 40, 35.0, "https://multimedia.bbycastatic.ca/multimedia/products/500x500/106/10630/10630290.jpg"));
        repository.save(new Robot("Robot 4", 10, 46.0, "https://i.pinimg.com/736x/62/2d/7f/622d7f9f08eb2e0b5d674c7028ef02cb--toys-r-us-toys--games.jpg"));
        repository.save(new Robot("Robot 5", 33, 12.5, "https://multimedia.bbycastatic.ca/multimedia/products/500x500/114/11456/11456576.jpg"));
        repository.save(new Robot("Robot 6", 12, 3.0, "https://images-na.ssl-images-amazon.com/images/I/41NLTPwM63L.jpg"));
        repository.save(new Robot("Robot 7", 40, 29.99, "https://img1.cgtrader.com/items/721222/a2746ceb37/large/boston-dynamics-dog-robot-3d-model-vray-3d-model-obj-fbx-mtl.png"));
        repository.save(new Robot("Robot 8", 90, 99.5, "https://i.pinimg.com/736x/38/a5/cd/38a5cddfdf44aca254e7a923ceeb4219--lego-spaceship-lego-robot.jpg"));
        repository.save(new Robot("Robot 9", 12, 99.5, "https://i.pinimg.com/736x/f8/26/64/f82664a1c5ac77bc91b96a4c32877ad4.jpg"));
        repository.save(new Robot("Robot 10", 44, 99.5, "https://images-na.ssl-images-amazon.com/images/I/51PdrxL1PhL._US500_.jpg"));
        repository.findAll().forEach(System.out::println);
    }
}
