package com.rbc.robot;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource

interface RobotRepository extends JpaRepository<Robot, Long>{

}
