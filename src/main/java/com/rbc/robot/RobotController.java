package com.rbc.robot;

import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
//@GetMapping("/robot")
@RequestMapping(value = "/robot")
public class RobotController {
    private RobotRepository repository;

    public RobotController(RobotRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Robot> getRobots() {

        return repository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Robot getRobot(@PathVariable("id") Long id) {
        return repository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Robot saveRobot(@RequestBody Robot robot) {
        repository.save(robot);
        return robot;
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Robot updateRobot(@RequestBody Robot robot) {
        Robot modifiedRobot = repository.findOne(robot.getId());
        modifiedRobot.setName(robot.getName());
        modifiedRobot.setQuantity(robot.getQuantity());
        modifiedRobot.setPrice(robot.getPrice());
        modifiedRobot.setImageUrl(robot.getImageUrl());
        repository.save(modifiedRobot);
        return modifiedRobot;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Boolean deleteRobot(@PathVariable("id") Long id) {
        Robot robot = repository.findOne(id);
        if (robot != null) {
            repository.delete(id);
            return true;
        } else {
            return false;
        }
    }
}