var app = angular.module('app', []);

app.controller('robotController', function($scope, $http, $location) {
    var apiURL = $location.absUrl() + "robot";
    var config = {};

    actionButton = document.getElementById("action-btn");

    $scope.robots = [];

    function loadRobots() {
        $http.get(apiURL, config).then(function (response) {
            $scope.robots = response.data;
        }, function error(response) {
            $scope.postResultMessage = "Error with status: " +  response.statusText;
        });
    }

    loadRobots();

    $scope.findRobot = function () {
        $scope.searchedRobot = true;
        $http.get(apiURL + "/" + $scope.robotId).then(function (response) {
            if(response.data.id) {
                $scope.robotFound = response.data;
                $scope.robots = [$scope.robotFound];
            } else {
                $scope.robotFound = null;
                $scope.robots = [];
            }
        }, function error(response) {
            $scope.robotFound = null;
        });
    };

    $scope.cancelFind = function () {
        $scope.robotId = "";
        $scope.searchedRobot = false;
        $scope.robotFound = null;
        loadRobots();
    };

    $scope.showRobot = function (id) {
        $http.get(apiURL + "/" + id).then(function (response) {
            console.log(response.data);
        }, function error(response) {
            $scope.postResultMessage = "Error with status: " +  response.statusText;
        });
    };

    $scope.editRobot = function (id) {
        $http.get(apiURL + "/" + id).then(function (response) {
            var robot = response.data;
            $scope.id = robot.id;
            $scope.name = robot.name;
            $scope.quantity = robot.quantity;
            $scope.price = robot.price;
            $scope.imageUrl = robot.imageUrl;


            actionButton.innerHTML = "Update";
            actionButton.classList.add("btn-success");

            var formSection = document.getElementById("add-new-robot-section");
            formSection.scrollIntoView();

            console.log(response.data);
        }, function error(response) {
            $scope.postResultMessage = "Error with status: " +  response.statusText;
        });
    };

    $scope.cancelEdit = function () {
        actionButton.innerHTML = "Add";
        clearForm();
    };

    function clearForm() {
        $scope.id = null;
        $scope.name = "";
        $scope.quantity = "";
        $scope.price = "";
        $scope.imageUrl = "";
    }

    $scope.deleteRobot = function (id) {
        $http.delete(apiURL + "/" + id).then(function (response) {
            loadRobots();
        }, function error(response) {
            $scope.postResultMessage = "Error with status: " +  response.statusText;
        });
    };


    $scope.submitForm = function() {
        if (!$scope.robotForm.$valid) {
            return;
        }

        var data = {
            name: $scope.name,
            quantity: $scope.quantity,
            price: $scope.price,
            imageUrl: $scope.imageUrl
        };
        if($scope.id) { // In case of an update
            data["id"] = $scope.id;
        }

        $http.post(apiURL, data, config).then(function (response) {
            clearForm();
            loadRobots();

        }, function error(response) {
            $scope.postResultMessage = "Error with status: " +  response.statusText;
        });
    }
});